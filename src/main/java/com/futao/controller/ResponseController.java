package com.futao.controller;

import com.futao.pojo.Address;
import com.futao.pojo.Request;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 测试响应数据
 */
@RestController
public class ResponseController {

//    @RequestMapping("/helloWorld")
//    public String hello() {
//        System.out.println("Hello World!");
//        return "Hello";
//    }
//
//    @RequestMapping("/getAddress")
//    public Address getAddress() {
//        Address address = new Address();
//        address.setProvince("广东");
//        address.setCity("深圳");
//        return address;
//    }
//
//    @RequestMapping("/listAddress")
//    public List<Address> listAddress() {
//        List<Address> list = new ArrayList<>();
//
//        Address address = new Address();
//        address.setProvince("广东");
//        address.setCity("深圳");
//
//        Address addressPls = new Address();
//        addressPls.setProvince("山东");
//        addressPls.setCity("菏泽");
//
//        list.add(address);
//        list.add(addressPls);
//
//        return list;
//    }

    @RequestMapping("/helloWorld")
    public Request<Object> hello() {
        System.out.println("Hello World!");
        return Request.success("Hello");
    }

    @RequestMapping("/getAddress")
    public Request<Address> getAddress() {
        Address address = new Address();
        address.setProvince("广东");
        address.setCity("深圳");
        return Request.success(address);
    }

    @RequestMapping("/listAddress")
    public Request<List<Address>> listAddress() {
        List<Address> list = new ArrayList<>();

        Address address = new Address();
        address.setProvince("广东");
        address.setCity("深圳");

        Address addressPls = new Address();
        addressPls.setProvince("山东");
        addressPls.setCity("菏泽");

        list.add(address);
        list.add(addressPls);

        return Request.success(list);
    }
}
