package com.futao.controller;

import com.futao.pojo.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

@RestController
public class EntityParams {
    // 实体参数
    @RequestMapping("/simplePojo")
    public String simplePojo(User user) {
        System.out.println(user.toString());
        return "OK";
    }

    @RequestMapping("/complexPojo")
    public String complexPojo(User user) {
        System.out.println(user);
        return "OK";
    }

    // 数组集合参数
    @RequestMapping("/arrayParam")
    public String complexPojo(String[] hobby) {
        System.out.println(Arrays.toString(hobby));
        return "OK";
    }
    // 数组集合
    @RequestMapping("/listParam")
    public String listParam(@RequestParam List<String> hobby) { // 对于复杂类型，Spring MVC无法自动进行绑定，需要用到@RequestParam注解
        System.out.println(hobby);
        return "OK";
    }
    // 日期 & 事件参数
    @RequestMapping("/dateParam")
    public String dateParam(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime updateTime) {
        System.out.println(updateTime);
        return "OK";
    }
    // JSON 格式
//    {
//        "name": "111",
//        "age": "222",
//        "address": {
//            "province": "北京市",
//            "city": "朝阳区"
//        }
//    }
    @RequestMapping("/jsonParam")
    public String jsonParam(@RequestBody User user) {
        System.out.println(user);
        return "OK";
    }
    // 路径参数
    @RequestMapping("/pathParam/{id}")
    public String pathParam(@PathVariable Integer id) {
        System.out.println(id);
        return "OK";
    }

    @RequestMapping("/pathParam/{id}/{name}")
//    public String pathParam(@PathVariable Integer id, @PathVariable String name) {
    public String pathParam2(@PathVariable Integer id, @PathVariable String name) {
        System.out.println(id + "-" + name);
        return "OK";
    }
}
