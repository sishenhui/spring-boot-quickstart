package com.futao.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试请求参数接收
 */
@RestController
public class RequestController {

    // 原始方式
//    @RequestMapping("/simpleParam")
//    public String simpleParam(HttpServletRequest request) {
//        // 获取请求参数
//        String name = request.getParameter("name");
//        String age = request.getParameter("age");
//
//        System.out.println(name + Integer.parseInt(age));
//
//        return "200 OK";
//    }

//    @RequestMapping("/simpleParam")
//    public String simpleParam2(@RequestParam(name = "name", required = false) String username, Integer age) {
//        System.out.println(username + ":" + age);
//        return "200 OK";
//    }

//    @RequestMapping("/simpleParam")
//    public String simpleParam3(String username, Integer age) {
//        System.out.println(username + ":" + age);
//        return "200 OK";
//    }
}
