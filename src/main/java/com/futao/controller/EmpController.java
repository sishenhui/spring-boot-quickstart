package com.futao.controller;

import com.futao.pojo.Emp;
import com.futao.pojo.Request;
import com.futao.service.EmpService;
import com.futao.service.impl.EmpServiceA;
import com.futao.utils.XmlParserUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class EmpController {
    private final EmpService empService = new EmpServiceA();

    @RequestMapping("/listEmp")
    public Request<List<Emp>> list() {
        // 1. 调用service, 获取数据
        List<Emp> empList = empService.listEmp();
        // 2. 响应数据
        return Request.success(empList);
    }

//    @RequestMapping("/listEmp")
//    public Request<List<Emp>> list() {
//        // 1. 加载并解析emp.xml
//        String file = Objects.requireNonNull(this.getClass().getClassLoader().getResource("emp.xml")).getFile();
//        System.out.println(file);
//        List<Emp> empList = XmlParserUtils.parse(file, Emp.class);
//
//        // 2. 对数据进行转换处理
//
//        empList.forEach(emp -> {
//            // <!-- 1: 男, 2: 女 -->
//            String gender = emp.getGender();
//            if ("1".equals(gender)) {
//                emp.setGender("男");
//            } else if ("2".equals(gender)) {
//                emp.setGender("女");
//            }
//
//            // <!-- 1: 讲师, 2: 班主任 , 3: 就业指导 -->
//            String job = emp.getJob();
//            if ("1".equals(job)) {
//                emp.setJob("讲师");
//            } else if ("2".equals(job)) {
//                emp.setJob("班主任");
//            } else if ("3".equals(job)) {
//                emp.setJob("就业指导");
//            }
//        });
//
//        // 3. 响应数据
//        return Request.success(empList);
//    }
}
