package com.futao.dao.impl;

import com.futao.dao.EmpDao;
import com.futao.pojo.Emp;
import com.futao.utils.XmlParserUtils;

import java.util.List;
import java.util.Objects;

public class EmpDaoA implements EmpDao {
    @Override
    public List<Emp> listEmp() {
        // 1. 加载并解析emp.xml
        String file = Objects.requireNonNull(this.getClass().getClassLoader().getResource("emp.xml")).getFile();
        List<Emp> empList = XmlParserUtils.parse(file, Emp.class);
        return empList;
    }
}
