package com.futao.pojo;

public class Request<T> {
    private Integer code;
    private String message;
    private T data;

    public Request() {
    }

    public Request(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public static <T> Request<T> success(T data) {
        return new Request<>(200, "Success", data);
    }

    public static <T> Request<T> success() {
        return new Request<>(200, "Success", null);
    }

    public static <T> Request<T> error(Integer code, String message) {
        return new Request<>(code, message, null);
    }

    @Override
    public String toString() {
        return "Request{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}